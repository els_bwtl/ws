#pragma once

#include <iostream>
#include <string>

using namespace std;

class DBRecord
{
public:
	DBRecord(); // ����������� �� ��������� ������� ������ ������
	DBRecord(string name, string surname, string passport); // ������� ������ � ������������ ������ �������� � ���������
	DBRecord(string c); // ������� ������ �� ������

	void print(); // ������� ������ � �������
	string getSurname(); // �������� ������� �� ������

	~DBRecord();

private:
	string plaintext; // ������
	string name; // ���
	string surname; // �������
	string passport; // �������
};

