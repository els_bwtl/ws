// ConsoleApplication21.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <openssl/conf.h>
#include <openssl/evp.h> 
#include <openssl/err.h> 
#include <openssl/aes.h>

#include <iostream>
#include <fstream>

#include <string>

#include "DBClass.h"

using namespace std;

void bruteforce(const char* filename, char pass_to_find[5]) {

	unsigned char *cryptedtext = new unsigned char[800]; // зашифрованная строка
	unsigned char *plaintext = new unsigned char[1000]; // расшифрованная
	unsigned char *iv = (unsigned char *)"0123456789012345"; // Рандомайзер
	
	int plaintext_len = 0; // длина строки после обработки
	int crypted_len = 0; // Длина шифра

	fstream in_crypted(filename, ios::binary | ios::in); // файловый поток
	if (in_crypted.is_open()) { // открываем файл
		in_crypted.read((char*)cryptedtext, 1000); // считываем шифр
		crypted_len = in_crypted.gcount(); // получаем кол во считанных символов
		in_crypted.close(); // закрываем файл
	}
	else {
		cerr << "не удается открыть файл" << endl;
		return;
	}

	int len = 0; // Длина строки
	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new(); // Создание структуры с настройками метода

	char* password = new char[256]; // шифр

	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10; y++) {

					pass_to_find[0] = i + '0'; pass_to_find[1] = j + '0'; pass_to_find[2] = x + '0'; pass_to_find[3] = y + '0';
					pass_to_find[4] = '\0'; // обозначаем конец строки

					sprintf(password, "0000000000000000000000000000%s", pass_to_find);

					EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, (unsigned char*)password, iv); // Инициализация методом AES, ключом и вектором

					EVP_DecryptUpdate(ctx, plaintext, &len, cryptedtext, crypted_len); // дешифровка

					plaintext_len = len; // длина до обработки
					EVP_DecryptFinal_ex(ctx, (unsigned char*)plaintext + len, &len); // Финальная обработка

					plaintext_len += len; // длина после
					plaintext[plaintext_len] = '\0'; // конец строки

					if (plaintext[0] == '{' && plaintext[1] == '\r') {
						cout << "Ключ: " << password 
							<< endl << "Данные: " << 
							plaintext << endl; // вывод в консоль
						EVP_CIPHER_CTX_free(ctx); // Освобождение памяти
						return; // завершить выполнение функции
					}
				}
			}
		}
	}

	EVP_CIPHER_CTX_free(ctx); // Освобождение памяти
}


int main()
{
	setlocale(LC_ALL, "Russian");

	char pass[5] = "0000"; // пароль
	bruteforce("5_encrypted", pass);

	char* password = new char[256];
	sprintf(password, "0000000000000000000000000000%s", pass);

	DBClass db1; // создаем БД пустую
	db1.load("5_encrypted", password); // заполняем записью с расшифровкой

	db1.add("Lisa", "Bunitzeva", "8998 123456");
	db1.add("Roma", "Kaverin", "8998 123456");

	cout << "Номер записи в БД: " << db1.find("8998 123456") << endl;

	db1.printAll(); // вывести все записи

	system("pause");

}